----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:29:39 06/28/2024 
-- Design Name: 
-- Module Name:    velocidadRPM - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL; 

entity velocidadRPM is 
	port(	clk: in std_logic; --15khz
			piAB: in std_logic_vector(1 downto 0);
			Display_Out: out std_logic_vector(3 downto 0));
end velocidadRPM;

architecture Behavioral of velocidadRPM is
	signal contador: integer := 0;
--	signal aux: integer := 0;
	signal velocidad: integer := 0;
	signal A_prev: std_logic := '0'; -- Previous state of A
	signal Display  : STD_LOGIC_VECTOR (3 downto 0):="0000";
	

	begin
		
		CONTADOR_PROCESS: process(clk)
		begin
--			if rising_edge(clk) then
--				if aux = 10 then
--					contador <= contador + 1;
--				else
--					contador <= 0;
--					aux <= 10;
--				end if;
--        end if;
		  
--			if piAB(0)'event and piAB(0) ='0' then -- Usar la señal A para medir el periodo
--				velocidad <= 66 * 500 * contador; -- 66us es el periodo de nuestro clk, 500 son los pulsos por vuelta, y max contador son los pulsos de clk que se contaron por periodo de pulso A.
--            aux <= 0;
--         end if;
--		end process;
		if rising_edge(clk) then
            if piAB(0) /= A_prev then -- Detect a change in signal A
                velocidad <= 1818/contador;         
                --hice regla de 3 simples, si 1vuelta -> 500*contador*66us
                --                            vel     <- 60 seg
                --entonces vel = 60/500*contador*66us = 1818/contador
                contador <= 0;
            else
                contador <= contador + 1;
            end if;
            A_prev <= piAB(0); -- Update previous state of A
        end if;
    end process;
		
	
----------------------------------------------------------------------------------

		DISPLAY_PROCESS: process(clk)
		begin
			if rising_edge(clk) then
				case velocidad is
                when 0 to 99999   => Display <= "0000";
                when 100000 to 199999 => Display <= "0001";
                when 200000 to 299999 => Display <= "0010";
                when 300000 to 399999 => Display <= "0011";
                when 400000 to 499999 => Display <= "0100";
                when 500000 to 599999 => Display <= "0101";
                when 600000 to 699999 => Display <= "0110";
                when 700000 to 799999 => Display <= "0111";
                when 800000 to 899999 => Display <= "1000";
                when others => Display <= "1001";
            end case;
			end if;
		end process;
------------------------------------------------------------

	Display_Out <= Display;
		
end Behavioral;

