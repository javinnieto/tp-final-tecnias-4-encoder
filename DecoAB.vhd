---------------------------------------------------------------------------------
-- UNIVERSIDAD TECNOL?GICA NACIONAL - FACULTAD REGIONAL CORDOBA
-- Carrera: INGENIERIA ELECTR?NICA.
-- Asignatura: T?CNICAS DIGITALES IV (ELECTIVA).
-- A?o: 2018


-- Pr?ctico n?xx: Decodificador de encoder de cuadratura.
-- Descripci?n: Aqu? se describe el funcionamiento del decodificador de entradas A-B. Se toma el valor actual
-- de las entradas AB cada vez que hay un flanco ascendente del clock y luego se realiz? una m?quina de estados
-- para verificar en cual de los posibles estados se encuentra y cual era el estado anterior, con esto se puede
-- definir si est? avanzando en sentido horario o antihorario. El sentido de giro se indica con 1 y 0 para horario
-- y antihorario respectivamente en la salida up_down y adem?s se env?a un pulso por la salida de pulsos. Si no 
-- se puede detectar el giro entonces se mantiene el valor anterior y la salida de pulsos queda en cero. En cada
-- flanco de bajada del clock se pone en cero la salida de pulsos para que solo se genere un pulso del ancho del
-- pulso del clock cuando se detecta el sentido de giro y un cambio en las entradas.
--

-- Colocar alg?n consejo para recordar en el futuro:
--	> Tener en cuenta de cerrar todos los case e if (end)
-- > Es buena pr?ctica utilizar un clock r?pido para generar un pulso simple a la salida "pulsos" en lugar de mantenerlo
-- siempre en alto porque sino es dif?cil detectar cambios de estado.
--
-- Nota:
--   ----------------                 /
--   /               \                /
--A  /               \ ----------------
--
--
--           ----------------        
--          /               \          
--B  -------/               \ ---------
--
--     E        E       E        E
--     A        A       A        A
--     B        B       B        B
--     1        1       0        0
--     0        1       1        0 
--
--   E      E        E      E        E
--   p      p        p      p        p
  


---------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity DecoAB is
   Port (	piAB 				:in		STD_LOGIC_VECTOR(1 downto 0);	--Entrada del encoder (A,B)
			clk,reset			:in		STD_LOGIC;	--Clock (15MHz)
			poPulsos_Up			:out	STD_LOGIC;	--Pulsos Up
			poPulsos_down 		:out 	STD_LOGIC; --Sentido Down
			Vect_1_Master_Out 	:out  	STD_LOGIC_VECTOR (6 downto 0); --Display1
            Vect_2_Master_Out 	:out  	STD_LOGIC_VECTOR (6 downto 0); --Display2
			Enable_display		:out 	STD_LOGIC_VECTOR (3 downto 0));	 -- Enable de los Display
end DecoAB;

architecture Behavioral of DecoAB is

   type state_type is (E_Reset,EAB00,EAB01,EAB10,EAB11,EAB00_pUp,EAB00_pDown,EAB01_pUp,EAB01_pDown,EAB10_pUp,EAB10_pDown,EAB11_pUp,EAB11_pDown); 
   signal state, next_state : state_type;

   --Declaraci?n de las se?ales internas que van a ser las salidas

	signal sPulso_up,sPulso_down	:	STD_LOGIC := '0';
	signal Display_Out: STD_LOGIC_VECTOR (3 downto 0):= "0000";

	component velocidadRPM
				port(	clk : in  STD_LOGIC;
						piAB : in  STD_LOGIC_VECTOR(1 downto 0);
						Display_Out: out STD_LOGIC_VECTOR(3 downto 0)
						);
	end component;

	component Codificador_BCDDisplay7Seg_DobleDigito
		 Port ( Vect_1_In : in  STD_LOGIC_VECTOR(3 downto 0);
				  clk : in STD_LOGIC;
				  Vect_1_Out : out  STD_LOGIC_VECTOR(6 downto 0);
				  Vect_2_Out : out  STD_LOGIC_VECTOR(6 downto 0);
				  Enable_display : out STD_LOGIC_VECTOR(3 downto 0));
	end component;


begin
 
----------------------------------------------------------------------------------------
   SYNC_PROC: process (clk) --Se encarga del estado RESET y de pasar state al nextState
   begin
      if (clk'event and clk = '1') then
         if (reset = '1') then
            state <= E_Reset;
--				
         else
            state <= next_state;
         end if;        
      end if;
   end process;
 
-----------------------------------------------------------------------------------------------

   OUTPUT_DECODE: process (state)
   begin
 		case (state) is
			when EAB00_pUp   => sPulso_up <='1'; sPulso_down <='0'; -- Estados donde es pulso Up (sentido horario)
			when EAB01_pUp   => sPulso_up <='1'; sPulso_down <='0';
			when EAB10_pUp   => sPulso_up <='1'; sPulso_down <='0';
			when EAB11_pUp   => sPulso_up <='1'; sPulso_down <='0';

			when EAB00_pDown   => sPulso_up <='0'; sPulso_down <='1'; -- Estados donde es pulso Down (sentido antihorario)
			when EAB01_pDown   => sPulso_up <='0'; sPulso_down <='1';
			when EAB10_pDown   => sPulso_up <='0'; sPulso_down <='1';
			when EAB11_pDown   => sPulso_up <='0'; sPulso_down <='1';

			when others => sPulso_up <='0'; sPulso_down <='0'; --Por ejemplo que est? parado
		end case;

   end process;
	
--------------------------------------------------------------------------
 
   NEXT_STATE_DECODE: process (state,piAB) --Definimos todos los estados siguientes teniendo en cuenta EABxx y piAB
   begin
		
      next_state <= state; --Declaramos el estado al siguiente estado
      
      case (state) is
         when E_Reset =>
				case piAB is
					when "00" => next_state <= EAB00;
					when "01" => next_state <= EAB01;
					when "10" => next_state <= EAB10;
					when "11" => next_state <= EAB11;
					when others => next_state <= E_Reset;
				end case;

         when EAB00 =>
				case piAB is
					when "00" => next_state <= EAB00;
					when "01" => next_state <=EAB00_pDown;
					when "10" => next_state <=EAB00_puP;
					when others => next_state <= E_Reset;
				end case;
            
         when EAB01 =>
				case piAB is
					when "00" => next_state <= EAB01_puP;
					when "01" => next_state <=EAB01;
					when "11" => next_state <=EAB01_pDown;
					when others => next_state <= E_Reset;
				end case;

         when EAB10 =>
				case piAB is
					when "10" => next_state <= EAB10;
					when "11" => next_state <=EAB10_pUp;
					when "00" => next_state <=EAB10_pDown;
					when others => next_state <= E_Reset;
				end case;

         when EAB11 =>
				case piAB is
					when "10" => next_state <= EAB11_pDown;
					when "01" => next_state <=EAB11_pUp;
					when "11" => next_state <=EAB11;
					when others => next_state <= E_Reset;
				end case;

         when EAB00_pDown => 
				if ( piAB = "01" ) then 
					next_state <= EAB01;
				else 
					next_state <= E_Reset;
				end if;
				
       
         when others => 
            next_state <= E_Reset; --Declaramos el estado por default para evitar problemas
      end case; 
   end process;
---------------------------------------------------------------------------

	Vel : velocidadRPM port map(clk => clk , --El llamado a la funcion velocidadRPM dentro del process
										piAB => piAB,
										Display_Out => Display_Out);

	COD1 : Codificador_BCDDisplay7Seg_DobleDigito port map(	Vect_1_In => Display_Out,
															clk => clk,
															Vect_1_Out => Vect_1_Master_Out,
															Vect_2_Out => Vect_2_Master_Out,
															Enable_display => Enable_display);

	poPulsos_Up<=sPulso_up;
	poPulsos_Down<=sPulso_down;
	
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------				




		
end Behavioral;

