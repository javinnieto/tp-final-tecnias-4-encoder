----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:57:52 05/19/2024 
-- Design Name: 
-- Module Name:    Codificador_BCDDisplay7Seg_DobleDigito - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Codificador_BCDDisplay7Seg_DobleDigito is
    Port ( Vect_1_In : in  STD_LOGIC_VECTOR(3 downto 0);
			  clk : in STD_LOGIC;
			  Vect_1_Out : out  STD_LOGIC_VECTOR(6 downto 0);
			  Vect_2_Out : out  STD_LOGIC_VECTOR(6 downto 0);
			  Enable_display : out STD_LOGIC_VECTOR(3 downto 0));
end Codificador_BCDDisplay7Seg_DobleDigito;

architecture Behavioral of Codificador_BCDDisplay7Seg_DobleDigito is
	signal Vect_1_Sig :  STD_LOGIC_VECTOR(6 downto 0); --Primeros 7 bits son del display las decenas (a1,b1,c1,d1,e1,f1,g1)
	signal Vect_2_Sig :  STD_LOGIC_VECTOR(6 downto 0);	--Segundos 7 bits son de las unidades (a2,b2,c2,d2,e2,f2,g2)
	signal Enable_signal: STD_LOGIC_VECTOR(3 downto 0):= "0111"; -- Enable para activar cada display
	
begin
	
	process(clk, Vect_1_In) is
	begin
		if clk'event and clk='1' then
		
		 if Enable_signal = "0111" then
			Enable_signal <= "1011";
		 elsif Enable_signal = "1011" then
			Enable_signal <= "0111";
		 end if;
		 
		 if Enable_signal = "0111" then
			case Vect_1_In is
				when "0000" =>  Vect_1_Sig<="1111110"; --Segundo Display 0
				when "0001" =>  Vect_1_Sig<="0110000"; --Segundo Display 1
				when "0010" =>  Vect_1_Sig<="1101101"; --Segundo Display 2
				when "0011" =>  Vect_1_Sig<="1111001"; --Segundo Display 3
				when "0100" =>  Vect_1_Sig<="0110011"; --Segundo Display 4
				when "0101" =>  Vect_1_Sig<="1011011"; --Segundo Display 5
				when "0110" =>  Vect_1_Sig<="1011111"; --Segundo Display 6
				when "0111" =>  Vect_1_Sig<="1110000"; --Segundo Display 7
				when "1000" =>  Vect_1_Sig<="1111111"; --Segundo Display 8
				when "1001" =>  Vect_1_Sig<="1110011"; --Segundo Display 9
				when others =>  Vect_1_Sig<="0000001";
			end case;
			
			elsif Enable_signal = "1011" then
				Vect_2_Sig<="1111110";
		
			
			end if;
		end if;
	end process;
	Vect_1_Out <= Vect_1_Sig;
	Vect_2_Out <= Vect_2_Sig;
	Enable_display <= Enable_signal;
	
end Behavioral;
