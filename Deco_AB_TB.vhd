--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   14:26:33 06/29/2024
-- Design Name:   
-- Module Name:   Y:/Practicos/TP_Final/Deco_AB_TB.vhd
-- Project Name:  TP_Final
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: DecoAB
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std.ALL;
 
ENTITY Deco_AB_TB1 IS
END Deco_AB_TB1;
 
ARCHITECTURE behavior OF Deco_AB_TB1 IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT DecoAB
    PORT(
         piAB : IN  std_logic_vector(1 downto 0);
         clk : IN  std_logic;
         reset : IN  std_logic;
         poPulsos_Up : OUT  std_logic;
         poPulsos_down : OUT  std_logic;
         Vect_1_Master_Out : OUT  std_logic_vector(6 downto 0);
         Vect_2_Master_Out : OUT  std_logic_vector(6 downto 0);
         Enable_display : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal piAB : std_logic_vector(1 downto 0) := (others => '0');
   signal clk : std_logic := '0';
   signal reset : std_logic := '0';

 	--Outputs
   signal poPulsos_Up : std_logic;
   signal poPulsos_down : std_logic;
   signal Vect_1_Master_Out : std_logic_vector(6 downto 0);
   signal Vect_2_Master_Out : std_logic_vector(6 downto 0);
   signal Enable_display : std_logic_vector(3 downto 0);

	 -- Clock period definitions
   constant clk_period : time := 100 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: DecoAB PORT MAP (
          piAB => piAB,
          clk => clk,
          reset => reset,
          poPulsos_Up => poPulsos_Up,
          poPulsos_down => poPulsos_down,
          Vect_1_Master_Out => Vect_1_Master_Out,
          Vect_2_Master_Out => Vect_2_Master_Out,
          Enable_display => Enable_display
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

    -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for clk_period*10;
		
		reset<='1';
      wait for clk_period*10;
		reset<='0';
------------------------------------------------------
     wait for clk_period*10;
	  piAB<="00";
     wait for clk_period*10;
	  piAB<="10";
     wait for clk_period*10;
	  piAB<="11";
     wait for clk_period*10;
	  piAB<="01";
	  
	  wait for clk_period*10;
	  piAB<="00";
     wait for clk_period*10;
	  piAB<="10";
     wait for clk_period*10;
	  piAB<="11";
     wait for clk_period*10;
	  piAB<="01";
	  
	  wait for clk_period*10;
	  piAB<="00";
     wait for clk_period*10;
	  piAB<="10";
     wait for clk_period*10;
	  piAB<="11";
     wait for clk_period*10;
	  piAB<="01";
--------------------------------------------------------------
     wait for clk_period*20;
	  piAB<="00";
     wait for clk_period*20;
	  piAB<="01";
     wait for clk_period*20;
	  piAB<="11";
     wait for clk_period*20;
	  piAB<="10";
	  
	  wait for clk_period*20;
	  piAB<="00";
     wait for clk_period*20;
	  piAB<="01";
     wait for clk_period*20;
	  piAB<="11";
     wait for clk_period*20;
	  piAB<="10";
	  
	  wait for clk_period*20;
	  piAB<="00";
     wait for clk_period*20;
	  piAB<="01";
     wait for clk_period*20;
	  piAB<="11";
     wait for clk_period*20;
	  piAB<="10";
-------------------------------------------------------------------------------------------
     wait for clk_period*10;
	  piAB<="00";
	  wait for clk_period*10;
	  piAB<="10";
     wait for clk_period*10;
	  piAB<="11";
     wait for clk_period*10;
	  piAB<="01";
	  
	  wait for clk_period*10;
	  piAB<="00";
    wait for clk_period*10;
	  piAB<="10";
     wait for clk_period*10;
	  piAB<="11";
     wait for clk_period*10;
	  piAB<="01";
	  
------------------------------------------------------------------------------------------
     wait for clk_period*10;
	  piAB<="00";
     wait for clk_period*10;
	  piAB<="01";
     wait for clk_period*10;
	  piAB<="00";
     wait for clk_period*10;
	  piAB<="01";
     wait for clk_period*10;
	  piAB<="00";
      wait;
   end process;

END;
